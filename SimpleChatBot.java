package automation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimpleChatBot {

	public static void main(String[] args) throws InterruptedException {
		GregorianCalendar time = new GregorianCalendar();
		int hour = time.get(Calendar.HOUR_OF_DAY);
		System.setProperty("webdriver.chrome.driver", "D:\\arka\\Documents\\LearnToCode\\Selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		WebDriverWait wait=new WebDriverWait(driver, 50);
		driver.navigate().to("https://web.whatsapp.com/");
		driver.manage().window().maximize();
		WebElement nextPage;
		nextPage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"side\"]/div[1]/div/label/div/div[2]")));
		nextPage.sendKeys("jio");
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"pane-side\"]/div[1]/div/div/div[1]/div/div/div[2]")).click();
		WebElement textBox;
		textBox = driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]"));
		if (hour < 12) {
			textBox.sendKeys("Good Morning,Reply *1* to schedule meeting or reply *2* to cancel meeting");
		}
		else if (hour >= 12 && hour < 17) {
			textBox.sendKeys("Good Afternoon,Reply *1* to schedule meeting or reply *2* to cancel meeting");
		}
		else {
			textBox.sendKeys("Good Evening,Reply *1* to schedule meeting or reply *2* to cancel meeting");
		}
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		List<WebElement> reply = new ArrayList<WebElement>();
		Thread.sleep(20000);
		reply = driver.findElements(By.className("_1Gy50"));
		int i = reply.size();
		String s = reply.get(i-1).getText();
		System.out.println(s);
		if (s.compareTo("1") == 0) {
			textBox.sendKeys("meeting scheduled two hours from now at "+ (hour + 2) + "hours" + Keys.ENTER);
		}
		else if (s.compareTo("2") == 0){
			textBox.sendKeys("meeting canceled" + Keys.ENTER);
		}
		else {
			textBox.sendKeys("wrong input" + Keys.ENTER);
		}
		//driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		
		
		

	}

}
